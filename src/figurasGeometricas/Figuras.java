package figurasGeometricas;
/* O uso da heran�a acarreta em diminui�ao de repeti��es do codigo,
  melhorando o codigo e fazendo com que ele fique mais enxuto e
  facil de entender, caso aconte�a algum erro em seu escopo.
 */
public class Figuras {
	
	private String quadrado;
	private String retangulo;
	private String trianguloRetangulo;
	private String trianguloAcutangolo;
	private String trianguloObtusangulo;
	private String losango;
	
	
	public String getQuadrado() {
		return quadrado;
	}
	public void setQuadrado(String quadrado) {
		this.quadrado = quadrado;
	}
	public String getRetangulo() {
		return retangulo;
	}
	public void setRetangulo(String retangulo) {
		this.retangulo = retangulo;
	}
	public String getTrianguloRetangulo() {
		return trianguloRetangulo;
	}
	public void setTrianguloRetangulo(String trianguloRetangulo) {
		this.trianguloRetangulo = trianguloRetangulo;
	}
	public String getTrianguloAcutangolo() {
		return trianguloAcutangolo;
	}
	public void setTrianguloAcutangolo(String trianguloAcutangolo) {
		this.trianguloAcutangolo = trianguloAcutangolo;
	}
	public String getTrianguloObtusangulo() {
		return trianguloObtusangulo;
	}
	public void setTrianguloObtusangulo(String trianguloObtusangulo) {
		this.trianguloObtusangulo = trianguloObtusangulo;
	}
	public String getLosango() {
		return losango;
	}
	public void setLosango(String losango) {
		this.losango = losango;
	}

}

