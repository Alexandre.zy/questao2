package figurasGeometricas;

public class forma extends Figuras{
	
	protected int lados;
	private int arestas;
	private int faces;
	protected Double angulo;
	
	
	public int getLados() {
		return lados;
	}
	public void setLados(int lados) {
		this.lados = lados;
	}
	public int getArestas() {
		return arestas;
	}
	public void setArestas(int arestas) {
		this.arestas = arestas;
	}
	public int getFaces() {
		return faces;
	}
	public void setFaces(int faces) {
		this.faces = faces;
	}
	public Double getAngulo() {
		return angulo;
	}
	public void setAngulo(Double angulo) {
		this.angulo = angulo;
	}

}
